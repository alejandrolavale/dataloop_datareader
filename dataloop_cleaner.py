import pandas as pd
import ast
from sdk_dataset_loader import get_json_filenames
from sweetreport_generation import get_csv_paths
pd.options.mode.chained_assignment = None

def clean_dataframe(df: pd.DataFrame) -> pd.DataFrame:

    cols_with_lists = ['main_attributes', 'secondary_attributes']

    for col in cols_with_lists:
        try: 
            df[col] = df[col].apply(eval)
        except:
            continue

    df['main_attributes_person'] = None
    df['main_attributes_annotation'] = None
    df['main_attributes_age'] = None
    df['main_attributes_gender'] = None
    df['secondary_attributes_person'] = None
    df['secondary_attributes_annotation'] = None
    df['tertiary_attributes_person'] = None
    df['tertiary_attributes_first_annotation'] = None
    df['tertiary_attributes_second_annotation'] = None
    df['upper_x_coordinates'] = None
    df['upper_y_coordinates'] = None
    df['lower_x_coordinates'] = None
    df['lower_y_coordinates'] = None
    
    for i in range(len(df)):
        
        try:
            df['main_attributes_person'][i] = df['main_attributes'][i][0]
        except:
            df['main_attributes_person'][i] = None
            
        try:
            df['main_attributes_annotation'][i] = df['main_attributes'][i][1]
        except:
            df['main_attributes_annotation'][i] = None

        try:
            df['main_attributes_age'][i] = df['main_attributes'][i][2]
        except:
            df['main_attributes_age'][i] = None

        try:
            df['main_attributes_gender'][i] = df['main_attributes'][i][3]
        except:
            df['main_attributes_gender'][i] = None

        try:
            df['secondary_attributes_person'][i] = df['secondary_attributes'][i][0]
        except:
            df['secondary_attributes_person'][i] = None

        try:
            df['secondary_attributes_annotation'][i] = df['secondary_attributes'][i][1]
        except:
            df['secondary_attributes_annotation'][i] = None

        try:
            df['tertiary_attributes'][i] = ast.literal_eval(df['tertiary_attributes'][i])
        except:
            continue

        try:
            df['upper_coordinates'][i] = ast.literal_eval(df['upper_coordinates'][i])
        except:
            continue

        try:
            df['lower_coordinates'][i] = ast.literal_eval(df['lower_coordinates'][i])
        except:
            continue

        df['tertiary_attributes_person'][i] = df['tertiary_attributes'][i].get('1')
        df['tertiary_attributes_first_annotation'][i] = df['tertiary_attributes'][i].get('4')
        df['tertiary_attributes_second_annotation'][i] = df['tertiary_attributes'][i].get('5')
        df['upper_x_coordinates'][i] = df['upper_coordinates'][i].get('x')
        df['upper_y_coordinates'][i] = df['upper_coordinates'][i].get('y')
        df['lower_x_coordinates'][i] = df['lower_coordinates'][i].get('x')
        df['lower_y_coordinates'][i] = df['lower_coordinates'][i].get('y')
        
    df.drop(['main_attributes', 'secondary_attributes', 'tertiary_attributes', 'upper_coordinates', 'lower_coordinates'], axis = 1, inplace = True)

    return df

if __name__ == "__main__":

    filepaths = get_json_filenames('/datasets/json/')
    filenames = [filename.replace('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/json/', '') for filename in filepaths]
    
    csvpaths = get_csv_paths()

    dfs = [pd.read_csv(csv) for csv in csvpaths]
    
    clean_dfs = [clean_dataframe(dataframe) for dataframe in dfs]

    for clean_df, filename in zip(clean_dfs, filenames):
        clean_df.to_csv(f'/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_csvs/df_{filename}.csv', index=False)
        clean_df.to_excel(f'/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_excels/df_{filename}.xlsx', index=False)

    #concated_df = pd.concat(clean_dfs)
    #concated_df.to_excel('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_excels/concated_df.xlsx', index=False)
    #concated_df.to_csv('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_csvs/concated_df.csv', index=False)
