import pandas as pd
import sweetviz as sv
import os

def get_csv_paths(csv_relative_path : str = '/datasets/csvs/') -> list:

    csv_dir = os.getcwd() + csv_relative_path

    list_csv_paths = [csv_dir + csv_file for csv_file in os.listdir(csv_dir) if csv_file.endswith('.csv')]

    return list_csv_paths


def filter_df(df : pd.DataFrame, list_of_labels : list = ['Attention', 'Expression', 'Head Movement', 'Speaking', 'Person', 'Engagement', 'Sentiment']) -> pd.DataFrame:

    df_cut = df[df['label'].isin(list_of_labels)]

    return df_cut 


def get_report(df : pd.DataFrame, filepath : str):
    
    my_report = sv.analyze(df)

    report_filepath = os.getcwd() + f'/datasets/reports/{filepath}.html'

    my_report.show_html(filepath = report_filepath)


if __name__ == "__main__":

    filepaths = get_csv_paths('/datasets/clean_csvs/')
    filenames = [filepath.replace('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_csvs/', '') for filepath in filepaths]

    dfs = [pd.read_csv(filepath) for filepath in filepaths]
    
    dfs_cut = [filter_df(df) for df in dfs]
    
    for df, filename in zip(dfs_cut, filenames):
        get_report(df, filepath = filename)
    
    