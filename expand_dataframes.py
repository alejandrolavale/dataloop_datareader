import pandas as pd
import os
from sweetreport_generation import get_csv_paths


def expand_frames_and_annotations(df : pd.DataFrame) -> pd.DataFrame:
    #filter with useful types of annotations
    df_filtered = df[df['label'].isin(['Expression', 'Attention', 'Head Movement', 'Person', 'Speaking'])]
    
    #dict with id : [annotation_values]
    ids = df_filtered['id'].unique()
    ids_values = []
    for row in df_filtered.itertuples():
        ids_values.append([row.label, row.main_attributes_person, row.main_attributes_annotation, row.main_attributes_age,
         row.main_attributes_gender, row.secondary_attributes_person, row.secondary_attributes_annotation, row.tertiary_attributes_person,
         row.tertiary_attributes_first_annotation, row.tertiary_attributes_second_annotation])
    
    cleaned_ids_list = []
    for values_list in ids_values:
        cleaned_values = [x for x in values_list if str(x) != 'nan']
        cleaned_ids_list.append(cleaned_values)

    annotations = dict(zip(ids, cleaned_ids_list))

    #create range of frames to create keys of second dict    
    min_frame = df_filtered['starting_frame'].min()
    max_frame = df_filtered['ending_frame'].max()
    frame_range = list(range(min_frame, max_frame))
    
    frame_values_list = []
    for row in df_filtered.itertuples():
        frame_values = [row.id, row.starting_frame, row.ending_frame]
        frame_values_list.append(frame_values)

    frames_to_ids = []
    for frames_list in frame_values_list:
        for frame in frame_range:
            if frame in range(frames_list[1], frames_list[2]):
                frames_to_ids.append([frame, frames_list[0]])
    
    frames =  [frames_to_ids[i][0] for i in range(len(frames_to_ids))]
    frames_ids = [frames_to_ids[i][1] for i in range(len(frames_to_ids))]
    frames_to_ids_dict = {}
    for frame, id in zip(frames, frames_ids):
        frames_to_ids_dict.setdefault(frame, []).append(id)
    
    df_frames_to_ids_dict = pd.DataFrame.from_dict(frames_to_ids_dict, orient='index')
    df_test = []
    for name, _ in df_frames_to_ids_dict.iteritems():
        column_series = df_frames_to_ids_dict[name].map(annotations)
        df_test.append(column_series)
    
    df_final = pd.DataFrame(df_test).T.sort_index()

    return df_final

if __name__ == '__main__':

    filepaths = get_csv_paths('/datasets/clean_csvs/')
    filenames = [filepath.replace('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_csvs/', '') for filepath in filepaths]

    dfs = [pd.read_csv(filepath) for filepath in filepaths]
    
    dfs_final = [expand_frames_and_annotations(df) for df in dfs[4:11]]
    print(dfs_final[0])