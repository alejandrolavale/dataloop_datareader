import dtlpy as dl
import pandas as pd
import json
import os

def get_datasets_ids_list(project: dl.Project) -> list:
    
    number_of_projects = len(project.datasets.list())
    datasets_ids_list = [project.datasets.list()[n].id for n in range(0, number_of_projects)]
    return datasets_ids_list

def download_dataset_list(dataset_ids_list: list, project: dl.Project, local_path: str, annotation_options = dl.VIEW_ANNOTATION_OPTIONS_JSON):

    datasets_list = [project.datasets.get(dataset_id = id) for id in dataset_ids_list]

    for dataset in datasets_list:
        dataset.download(local_path=local_path, annotation_options = annotation_options)


def get_json_filenames(json_relative_folder_path: str = '/json') -> list:

    cwd = os.getcwd()
    json_folder_path = cwd + json_relative_folder_path
    
    list_json_filepaths = [cwd + json_relative_folder_path + jsonfile for jsonfile in os.listdir(json_folder_path) if jsonfile.endswith('.json')]

    return list_json_filepaths


def json_clean(file_path: str) -> pd.DataFrame:

    file = open(file_path)
    jsonContent = file.read()
    data = json.loads(jsonContent)
    
    number_of_annotations = len(data['annotations'])

    filename = data['filename'].replace('/', '')

    list_of_ids = []
    list_of_starting_frames = []
    list_of_ending_frames = []
    list_of_starting_seconds = []
    list_of_ending_seconds = []
    list_of_upper_coordinates = []
    list_of_lower_coordinates = []
    list_of_annotation_labels = []
    list_of_main_attributes = []
    list_of_secondary_attributes = []
    list_of_tertiary_attributes = []

    for n in range(0, number_of_annotations):
        
        id = data.get('annotations')[n].get('id')
        list_of_ids.append(id)
        
        extracted_starting_frames = data.get('annotations')[n].get('metadata').get('system').get('frame')
        list_of_starting_frames.append(extracted_starting_frames)
        
        extracted_ending_frames = data.get('annotations')[n].get('metadata').get('system').get('endFrame')
        list_of_ending_frames.append(extracted_ending_frames)
        
        extracted_starting_seconds = data.get('annotations')[n].get('metadata').get('system').get('startTime')
        list_of_starting_seconds.append(extracted_starting_seconds)
        
        extracted_ending_seconds = data.get('annotations')[n].get('metadata').get('system').get('endTime')
        list_of_ending_seconds.append(extracted_ending_seconds)
        
        annotation = data.get('annotations')[n].get('label')
        list_of_annotation_labels.append(annotation)

        main_attributes = data.get('annotations')[n].get('attributes')
        list_of_main_attributes.append(main_attributes)

        try:
            secondary_attributes = data.get('annotations')[n].get('metadata').get('system').get('snapshots_')[0].get('attributes')
            list_of_secondary_attributes.append(secondary_attributes)            
        except:
            list_of_secondary_attributes.append(list())
            
        try:
            tertiary_attributes = data.get('annotations')[n].get('metadata').get('system').get('snapshots_')[0].get('namedAttributes')
            list_of_tertiary_attributes.append(tertiary_attributes)        
        except:
            list_of_tertiary_attributes.append(dict())

        try:
            upper_coordinates = data.get('annotations')[n].get('coordinates')[0]
            list_of_upper_coordinates.append(upper_coordinates)
        except:
            list_of_upper_coordinates.append(dict())

        try:
            lower_coordinates = data.get('annotations')[n].get('coordinates')[1]
            list_of_lower_coordinates.append(lower_coordinates)
        except:
            list_of_lower_coordinates.append(dict())


    df_dict = {'filename': filename, 'id': list_of_ids, 'label': list_of_annotation_labels, 'starting_frame': list_of_starting_frames,
     'ending_frame': list_of_ending_frames, 'starting_seconds': list_of_starting_seconds, 'ending_seconds': list_of_ending_seconds,
     'upper_coordinates': list_of_upper_coordinates, 'lower_coordinates': list_of_lower_coordinates,
     'main_attributes': list_of_main_attributes, 'secondary_attributes': list_of_secondary_attributes,
     'tertiary_attributes': list_of_tertiary_attributes}
    
    df = pd.DataFrame(df_dict)

    df['total_seconds_annotated'] = df['ending_seconds'] - df['starting_seconds']
    df['total_frames_annotated'] = df['ending_frame'] - df['starting_frame']
    
    return df

if __name__ == "__main__":

    if dl.token_expired():
        dl.login_m2m(email='alejandro.lavale@uniphore.com', password='PQMwGXg6c8BF5cJS')

    project = dl.projects.get(project_id='cf5387b1-744d-41e8-9c09-a85746c7bcc0')
    local_path = r'/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets'

    #datasets_ids_list = get_datasets_ids_list(project)
    #dataset_custom_ids_list = get_datasets_ids_list(project)
    dataset_custom_ids_list = ['6172c72c022770a961613384', '6172cd6a7e42bf7e948d8687', '6172d524177a91c3005dd115', '6172e0a72ed079ea7fc6782c', '6172f6a02ed079f8a2c688a2', '617302f12db0cabe28692b4a', '61730c251b5268f99bbb452d']
    second_sentiment_video = ['6171950bac542e65fa92b596']
    #print(dataset_custom_ids_list)

    download_dataset_list(second_sentiment_video, project, local_path)

    file_paths = get_json_filenames('/datasets/json/')
    file_names = [file_name.replace('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/json/', '') for file_name in file_paths]

    dfs = [json_clean(filepath)for filepath in file_paths]
    
    for df, filename in zip(dfs, file_names):
        df.to_csv(f'/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/csvs/df_{filename}.csv', index=False)
        df.to_excel(f'/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/excels/df_{filename}.xlsx', index=False)

    #concated_df = pd.concat(dfs)

    #concated_df.to_excel('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/excels/concated_df.xlsx', index=False)
    #concated_df.to_csv('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/csvs/concated_df.csv', index=False)
