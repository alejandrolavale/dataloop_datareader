import cv2

def frame_extractor():
    list_of_frames = [1000, 2000, 3000, 4000, 6000, 7000]

    vidcap = cv2.VideoCapture("./datasets/items/GMT20210827-153228_Recording_gallery_1280x720 (1).mp4")
    number_frames = vidcap.get(7)
    
    for number in list_of_frames:
        vidcap.set(1, number)
        ret, frame = vidcap.read()
        
        cv2.imwrite(f"Frame_{number}.jpg", frame)


if __name__ == '__main__':

    frame_extractor()