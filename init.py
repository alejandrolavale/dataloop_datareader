from pathlib import Path

_ROOT = Path(__file__).parent.parent


def get_relative_path(*relative_path: str) -> Path:
    full_path = _ROOT
    for sub_path in relative_path:
        full_path = full_path / sub_path
    return full_path