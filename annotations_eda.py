import pandas as pd
from sweetreport_generation import get_csv_paths
from typing import List

def get_sum_seconds_annotated(df : pd.DataFrame) -> int:
    total_seconds = df['total_seconds_annotated'].sum()
    return total_seconds

def clean_dataframes_columns(df : pd.DataFrame, list_of_cols : List = ['Attention', 'Expression', 'Head Movement']) -> pd.DataFrame:
    df_filtered = df[df['label'].isin(list_of_cols)]
    return df_filtered

if __name__ == '__main__':

    #Read and load all csvs
    filepaths = get_csv_paths('/datasets/clean_csvs/')
    filenames = [filename.replace('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_csvs/', '') for filename in filepaths]
    dfs = [pd.read_csv(filepath) for filepath in filepaths]
    dfs_filtered = [clean_dataframes_columns(df, list_of_cols=['Attention', 'Expression', 'Head Movement', 'Speaking', 'Person']) for df in dfs]

    total_seconds_per_video = [get_sum_seconds_annotated(df) for df in dfs]
    total_seconds_in_total = 0
    for number in total_seconds_per_video:
        total_seconds_in_total += number
    
    total_seconds_per_video_filtered = [get_sum_seconds_annotated(df) for df in dfs_filtered]
    total_seconds_in_total_filtered = 0
    for number in total_seconds_per_video_filtered:
        total_seconds_in_total_filtered += number

    print(total_seconds_in_total_filtered/3600)