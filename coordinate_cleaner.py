import pandas as pd
from sweetreport_generation import get_csv_paths

def clean_coordinates(df : pd.DataFrame) -> pd.DataFrame:
    df_label = df[df['label'] == 'Person']
    df_coordinates = df_label.loc[:, ['filename', 'starting_frame', 'ending_frame', 'main_attributes_person',
                                      'secondary_attributes_person', 'tertiary_attributes_person',
                                      'upper_x_coordinates', 'upper_y_coordinates',
                                      'lower_x_coordinates', 'lower_y_coordinates']]
    df_coordinates_clean = df_coordinates.dropna(axis=0, subset=['main_attributes_person', 'secondary_attributes_person',
                                                                 'tertiary_attributes_person', 'upper_x_coordinates', 
                                                                 'upper_y_coordinates', 'lower_x_coordinates', 'lower_y_coordinates'],
                                                                 how='all')
    return df_coordinates_clean

if __name__ == '__main__':

    filepaths = get_csv_paths('/datasets/clean_csvs/')
    filenames = [filepath.replace('/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/clean_csvs/', '') for filepath in filepaths]

    dfs = [pd.read_csv(filepath) for filepath in filepaths]

    dfs_coordinates = [clean_coordinates(df) for df in dfs]

    for df_coordinates, filename in zip(dfs_coordinates, filenames):
        df_coordinates.to_csv(f'/mnt/c/Users/AlejandroLavale/Desktop/ERL + Uniphore/September/Dataloop-testing/datasets/coordinates_csvs/df_{filename}.csv', index=False)

    
    