import pandas as pd
import cv2
from sweetreport_generation import get_csv_paths

def frame_extractor(video_path: str, list_of_frames: pd.Series):

    path_to_folder = './datasets/items/'
    full_video_path = path_to_folder + video_path
    splitted_full_video_path = full_video_path.split('/')
    filename = splitted_full_video_path[-1]
    vidcap = cv2.VideoCapture(full_video_path)

    for number_of_frame in list_of_frames:
        vidcap.set(1, number_of_frame)
        _, frame = vidcap.read()
        resized_frame = cv2.resize(frame, (640, 360), interpolation = cv2.INTER_AREA)
        cv2.imwrite(f"./datasets/frames/{filename}_{number_of_frame}.jpg", resized_frame)

#def frame_cropper()

if __name__ == '__main__':

    csvs_paths = get_csv_paths('/datasets/coordinates_csvs/')

    dfs_coordinates = [pd.read_csv(file) for file in csvs_paths]

    keys_list = [dfs_coordinates[i]['filename'].unique()[0] for i in range(len(dfs_coordinates))]
    
    values_list = [dfs_coordinates[i]['starting_frame'].unique().tolist() for i in range(len(dfs_coordinates))]

    #files_frames_dict = {keys_list[i]:values_list[i] for i in range(len(keys_list))}

    frame_extractor(keys_list[2], values_list[2])